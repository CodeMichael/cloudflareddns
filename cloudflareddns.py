#!/usr/bin/env python
'''
update DNS record with local public IP address
when put on a schedule this acts as a
Dynamic DNS service

Requirements:
This tool requires the following libraries:
    - dnspython
    - cloudflare
This tool expect 4 environment variables:
    - DNS_ZONE      == the cloudflare dns zone (your primary domain)
    - DNS_SUBDOMAIN == the subdomain of the primary domain
    - CF_API_EMAIL  == the email of your cloudflare account
    - CF_API_KEY    == the api key of your cloudflare account
'''

import logging
import os
# https://github.com/cloudflare/python-cloudflare
import CloudFlare as CF
from dns import resolver
ZONE = os.environ['DNS_ZONE']
SUBDOMAIN = os.environ['DNS_SUBDOMAIN'] + ZONE
MYIP = "myip.opendns.com"
NAMESERVER = "208.67.222.222"
if 'LOG_LEVEL' in os.environ:
    LOGLEVEL = os.environ['LOG_LEVEL']
else:
    LOGLEVEL = "INFO"

def main():
    ''' primary app '''
    cloudflare = CF.CloudFlare()
    res = resolver.Resolver()
    res.nameservers = [NAMESERVER]
    dns = res.query(MYIP)
    for rdata in dns:
        logging.debug(rdata.address)
        curr_address = rdata.address
        logging.debug(curr_address)
    zones = cloudflare.zones.get(params={'name': ZONE}) #pylint: disable=no-member
    for zone in zones:
        records = cloudflare.zones.dns_records.get(zone['id']) #pylint: disable=no-member
        for record in records:
            if record['name'] == SUBDOMAIN:
                logging.debug(record)
                curr_record = record['content']
                if curr_address == curr_record:
                    print("current IP and cloudflare IP match, no need to update")
                    exit(0)
                else:
                    print("current IP and cloudflare IP are different.")
                    print("updating cloudflare.")
                    new_record = {
                        'name': record['name'],
                        'type': record['type'],
                        'content': curr_address}
                    logging.debug(new_record)
                    response = cloudflare.zones.dns_records.delete(zone['id'], record['id']) #pylint: disable=no-member
                    response = cloudflare.zones.dns_records.post(zone['id'], data=new_record) #pylint: disable=no-member
                    logging.debug(response)
                    print("Record updated!")

    exit(0)

if __name__ == '__main__':
    logging.basicConfig(level=LOGLEVEL)
    main()
