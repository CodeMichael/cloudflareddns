# Cloudflare DDNS

This is a simple tool for updating a cloudflare A record with the current
machines local egress IP.  This is used by the author for dynamic dns on a
home LAN.

This tool is written in python and uses the `cloudflare` and `dnspython`
libs.  It takes inputs in the form of environment variables.

Feedback welcome.